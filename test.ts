/// <reference types="mocha"/>
/// <reference types="node" />
import xs, {Stream} from 'xstream';
import dropRepeatsByKeys from './index';
import * as assert from 'assert';

describe('dropRepeats (extra)', () => {
  it('should drop consecutive duplicate numbers (as events)', (done: any) => {
    const stream = xs
      .of(
        {age: 1},
        {age: 2},
        {age: 1},
        {age: 1},
        {age: 2},
        {age: 3},
        {age: 4},
        {age: 4},
      )
      .compose(dropRepeatsByKeys(['age']));
    const expected = [
      {age: 1},
      {age: 2},
      {age: 1},
      {age: 2},
      {age: 3},
      {age: 4},
    ];

    stream.addListener({
      next: x => {
        assert.equal(JSON.stringify(x), JSON.stringify(expected.shift()));
      },
      error: (err: any) => done(err),
      complete: () => {
        assert.equal(expected.length, 0);
        done();
      },
    });
  });

  it('should complete when input completes', (done: any) => {
    const stream = xs.of({age: 1}).compose(dropRepeatsByKeys(['age']));
    const expected = [{age: 1}];

    stream.addListener({
      next: x => {
        assert.equal(JSON.stringify(x), JSON.stringify(expected.shift()));
      },
      error: (err: any) => done(err),
      complete: () => {
        assert.equal(expected.length, 0);
        done();
      },
    });
  });

  it('should support debugMode', (done: any) => {
    const stream = xs
      .of({age: 1}, {age: 1}, {age: 2})
      .compose(dropRepeatsByKeys(['age'], true));
    const expected = [{age: 1}, {age: 2}];

    stream.addListener({
      next: x => {
        assert.equal(JSON.stringify(x), JSON.stringify(expected.shift()));
      },
      error: (err: any) => done(err),
      complete: () => {
        assert.equal(expected.length, 0);
        done();
      },
    });
  });

  it('should supports picker functions', (done: any) => {
    const stream = xs
      .of({age: 1}, {age: 1}, {age: 2})
      .compose(dropRepeatsByKeys([x => x.age], true));
    const expected = [{age: 1}, {age: 2}];

    stream.addListener({
      next: x => {
        assert.equal(JSON.stringify(x), JSON.stringify(expected.shift()));
      },
      error: (err: any) => done(err),
      complete: () => {
        assert.equal(expected.length, 0);
        done();
      },
    });
  });

  it('should return the correct TypeScript types', (done: any) => {
    type Stuff = {age: number};
    const first: Stream<Stuff> = xs.of({age: 10});
    const second: Stream<Stuff> = first.compose(dropRepeatsByKeys(['age']));
    done();
  });
});
