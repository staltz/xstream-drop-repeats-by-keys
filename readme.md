# `xstream-drop-repeats-by-keys`

```
pnpm install --save xstream-drop-repeats-by-keys
```

Like xstream `dropRepeats`, but acts on a stream of objects, and takes a list of keys as arguments. Those keys are the ones we want to check for equality.

## description

An xstream operator to filter out duplicate objects judging by some keys.

Marble diagram:

Example:

```js
import dropRepeatsByKeys from 'xstream-drop-repeats-by-keys'

const stream = xs.of({age: 27, name: 'Adele'}, {age: 28, name: 'Adele'})
  .compose(dropRepeatsByKeys(['name']))

stream.addListener({
  next: i => console.log(i),
  error: err => console.error(err),
  complete: () => console.log('completed')
})
```

```text
> {age: 27, name: 'Adele'}
> completed
```

For debug purposes, you can also pass a boolean as the second argument, this will make the operator work in debug mode, so it will output to `console.log` what the difference in fields was when one of the fields determined by the `keys` is different. By default, the operator has this second argument set to `false`. See below:

```js
import dropRepeatsByKeys from 'xstream-drop-repeats-by-keys'

const stream = xs.of({name: 'a'}, {name: 'b'}, {name: 'b'})
  .compose(dropRepeatsByKeys(['name'], true))

stream.addListener({
  next: i => console.log(i),
  error: err => console.error(err),
  complete: () => console.log('completed')
})
```

```text
> {name: 'a'}
> xstream-drop-repeats-by-keys found different values for the key 'name': a !== b
> {name: 'b'}
> completed
```

## License

MIT

