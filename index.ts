import {Operator, Stream} from 'xstream';
const empty = {};

type Picker<T> = (t: T) => unknown;

export class DropRepeatsByKeysOperator<T, K extends keyof T>
  implements Operator<T, T> {
  public type = 'dropRepeatsByKeys';
  public out: Stream<T> = null as any;
  private v: T = empty as any;

  constructor(
    public ins: Stream<T>,
    public specifiers: Array<K | Picker<T>>,
    public debugMode: boolean,
  ) {}

  _start(out: Stream<T>): void {
    this.out = out;
    this.ins._add(this);
  }

  _stop(): void {
    this.ins._remove(this);
    this.out = null as any;
    this.v = empty as any;
  }

  isEq(t1: T, t2: T) {
    for (const specifier of this.specifiers) {
      if (typeof specifier === 'string') {
        const key = specifier as K;
        const p1 = t1[key]
        const p2 = t2[key]
        if (p1 !== p2) {
          if (this.debugMode) {
            console.log(
              'xstream-drop-repeats-by-keys found different values ' +
                `for the key '${key}': ${p1} !== ${p2}`,
            );
          }
          return false;
        }
      } else {
        const picker = specifier as Picker<T>;
        const p1 = picker(t1);
        const p2 = picker(t2);
        if (p1 !== p2) {
          if (this.debugMode) {
            console.log(
              'xstream-drop-repeats-by-keys found different values ' +
                `for the picker ${picker}: ${p1} !== ${p2}`,
            );
          }
          return false;
        }
      }
    }
    return true;
  }

  _n(t: T) {
    const u = this.out;
    if (!u) return;
    const v = this.v;
    if (v !== empty && this.isEq(v, t)) return;
    this.v = t;
    u._n(t);
  }

  _e(err: any) {
    const u = this.out;
    if (!u) return;
    u._e(err);
  }

  _c() {
    const u = this.out;
    if (!u) return;
    u._c();
  }
}

/**
 * Drops consecutive duplicate objects in a stream, where equality is determined
 * by some keys.
 *
 * @param {Array} keys an array of object keys (strings) on which we will test
 * for strict equality, or picker functions
 * @param {Boolean} debugMode a boolean indicating whether we want to run this
 * operator in debug mode
 * @return {Stream}
 */
export default function dropRepeatsByKeys<T, K extends keyof T>(
  keys: Array<K | Picker<T>>,
  debugMode: boolean = false,
): (ins: Stream<T>) => Stream<T> {
  return function dropRepeatsByKeysOperator(ins: Stream<T>): Stream<T> {
    return new Stream<T>(
      new DropRepeatsByKeysOperator<T, K>(ins, keys, debugMode),
    );
  };
}
